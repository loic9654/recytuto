package fr.loirotte.recytuto;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class DetailItem extends AppCompatActivity {
      @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Bundle extras = getIntent().getBundleExtra("BUNDLE_EXTRAS");

        ((TextView)findViewById(R.id.textdetail)).setText(extras.getString("EXTRA_QUOTE"));
        ((TextView)findViewById(R.id.lbl_quote_attribution)).setText(extras.getString("EXTRA_ATTR"));
    }
}